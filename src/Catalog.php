<?php


namespace thinkcart;


use think\admin\Controller;

class Catalog extends Controller
{

    protected function initialize(){
        parent::initialize();
        $this->setLangs();
    }


    /**
     * 设置语言
     */
    protected function setLangs($module = 'admin'){
        $path = $this->app->getBasePath().'lang'.DIRECTORY_SEPARATOR.$this->app->lang->getLangSet().DIRECTORY_SEPARATOR.$module;
        foreach(glob($path.DIRECTORY_SEPARATOR.'*.php') as $file){
            $this->app->lang->load($file);
        }
    }
}